# git-annex repo of free stuff

This repo just exists for collecting links to free (and legally available)
resources.

To use:

1. Clone this repo
2. Read https://git-annex.branchable.com/walkthrough/
3. `git annex get` the files you are interested in

